import unicodedata

class SCREEN:
    MINI_COLUMN = 5
    def __init__(self, width):
        self.screenWidth = width
    
    def __lenMessage(self, msg):
        def isChinese(chr):
            status = unicodedata.east_asian_width(chr)
            return status == 'F'or status == 'W'
        len = 0
        for chr in msg:
            len += 1
            if(isChinese(chr)): len += 1
        return len
    
    def __wrap(self, msg, width):
        def ellipsis(title):
            titleEllipsis = ""
            length = 0
            for chr in title:
                if(length + self.__lenMessage(chr) + 3 > width):
                    titleEllipsis += "..."
                    break
                titleEllipsis += chr
                length += self.__lenMessage(chr)
            return titleEllipsis
        def wrapLine(line):
            lineLen = 0
            ret = ""
            for chr in line:
                if(chr == '\n' or lineLen + self.__lenMessage(chr) > width):
                    content.append(ret)
                    ret = ""
                    lineLen = 0
                if(chr != '\n'):
                    ret += chr
                    lineLen += self.__lenMessage(chr)
            if(len(ret)):content.append(ret)
        
        content = []
        title = None
        msgtype = type(msg)
        if(msgtype == list or msgtype == tuple):
            title = msg[0]
            msg = list(msg[1:])
        elif(msgtype == dict):
            title = list(msg.keys())[0]
            msg = msg[title]
        elif(msgtype == int):
            msg = str(msg)
        if(type(msg) != list):msg = [msg]
        for line in msg:
            wrapLine(line)
        if(title is not None):title = ellipsis(title)
        return {'title':title,'content':content}
    
    def print(self, *args):
        def printTitle():
            line = ""
            for i in range(contentCount):
                title = outputArr[i]['title']
                if(title is not None):
                    line += title
                    offset = contentWidthLimit - self.__lenMessage(title)
                    if(offset > 0):line += " " * offset
                    if(self.__lenMessage(title) > len(title)):line += "\t"
                else:
                    line += " " * contentWidthLimit
                line += "\t"
            print(line)
        def printContent():
            for index in range(contentMaxLength):
                line = ""
                for i in range(contentCount):
                    offset = 0
                    content = None
                    if(index < len(outputArr[i]["content"])):content = outputArr[i]["content"][index]
                    if(content is not None):
                        offset = contentWidthLimit - self.__lenMessage(content)
                        line += content
                        if(offset > 0):line += " " * offset
                        # if(self.__lenMessage(content) > len(content)):line += "\t"
                    else:
                        line += " " * contentWidthLimit
                    line += "\t"
                print(line)
        contentCount = len(args)
        contentWidthLimit = (self.screenWidth - contentCount + 1) // contentCount
        outputArr = []
        contentMaxLength = 0
        if(contentWidthLimit < SCREEN.MINI_COLUMN):contentWidthLimit = SCREEN.MINI_COLUMN
        for msg in args:
            content = self.__wrap(msg, contentWidthLimit)
            outputArr.append(content)
            contentMaxLength = max(contentMaxLength, len(content["content"]))
        printTitle()
        printContent()

if __name__=='__main__':
    screen = SCREEN(100)
    screen.print("现在看到的是测试信息，内容可以是英文ABCD,也可以是中文",["title","现在看到的是测试信息，内容可以是英文ABCD,也可以是中文。现在看到的是测试信息，内容可以是英文ABCD,也可以是中文。现在看到的是测试信息，内容可以是英文ABCD,也可以是中文。"],{"这是标题":"123456790"})