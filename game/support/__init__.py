import subprocess
try:
    from PIL import Image,ImageFont,ImageDraw
except Exception as e:
    module = None
    if(e.name == "PIL"):
        module = "pillow"
    if(module):
        process = subprocess.Popen(["python","-m","pip","install",module])
        process.wait()
