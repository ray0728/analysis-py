import getopt,sys,os,time
from random import choice
from support.screen import SCREEN
#from PIL import Image,ImageFont,ImageDraw

class MAZE:
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3
    PADDING = 5
    DIRECT_EMT = ["⇡","⇣","⇠","⇢"]
    COLOR_EMT = ["#434343", "#FFFFFF", "#3C78D8", "#FFFFFF"]
    
    def __init__(self, row, column) -> None:
        self.row = row
        self.column = column
        self.table = None
        self.times = [0, 0]
    
    def create(self):
        def getWallRules(row, column):
            kptwall = [MAZE.UP, MAZE.RIGHT, MAZE.DOWN, MAZE.LEFT]
            index = row * self.column + column
            if(index < self.column):kptwall.remove(MAZE.UP)
            if((index % self.column + 1) == self.column):kptwall.remove(MAZE.RIGHT)
            if(index + self.column >= self.row * self.column):kptwall.remove(MAZE.DOWN)
            if(index % self.column == 0):kptwall.remove(MAZE.LEFT)
            return kptwall
        def newTable(row, column):
            tables = []
            index = 0
            for r in range(self.row):
                columns = []
                for c in range(self.column):
                    room = {"id":index, "row":r, "column":c}
                    room["wid"] = getWallRules(r, c)
                    columns.append(room)
                    index += 1
                tables.append(columns)
            return tables
        def connectNeighbor(room, wid, broken=True):
            row = room["row"]
            column = room["column"]
            if(wid == MAZE.UP):
                wid = MAZE.DOWN
                row -= 1
            elif(wid == MAZE.RIGHT):
                wid = MAZE.LEFT
                column += 1
            elif(wid == MAZE.DOWN):
                wid = MAZE.UP
                row += 1
            elif(wid == MAZE.LEFT):
                wid = MAZE.RIGHT
                column -= 1
            neighbor = self.table[row][column]
            if(broken):neighbor["wid"].remove(wid)
            return neighbor
        def getValidWalls(room, lst):
            ret = []
            for wid in room["wid"]:
                neighbor = connectNeighbor(room, wid, False)
                if(neighbor not in lst):ret.append(wid)
            return ret
        def brokenWall(room, historyLst, reachLst):
            walls = getValidWalls(room, reachLst)
            if(walls):
                wid = choice(walls)
                room["wid"].remove(wid)
                neighbor = connectNeighbor(room, wid)
                historyLst.append(neighbor)
                reachLst.append(neighbor)
                brokenWall(neighbor,historyLst, reachLst)
        def excludeNoWallRoom(lst, refLst):
            for room in lst.copy():
                if(not getValidWalls(room, refLst)):lst.remove(room)
        start = time.perf_counter()
        reachableRooms = []
        viewHistoryRooms = []
        self.table = newTable(self.row, self.column)
        room = self.table[0][0]
        reachableRooms.append(room)
        viewHistoryRooms.append(room)
        threshold = self.row * self.column // 2
        while(len(reachableRooms) < (self.row * self.column)):
            brokenWall(room, viewHistoryRooms, reachableRooms)
            if(len(viewHistoryRooms) >= threshold):excludeNoWallRoom(viewHistoryRooms, reachableRooms)
            if(not viewHistoryRooms):break
            room = choice(viewHistoryRooms)
        self.times[0] = time.perf_counter() - start
        self.__findRoute()
    
    def __findRoute(self):
        def getMiniFValueArray(lst1, lst2):
            f1 = self.row * self.column
            f2 = f1
            mini = None
            if(lst1):f1 = lst1[0]["f"]
            if(lst2):f2 = lst2[0]["f"]
            if(f1 <= f2):
                mini = lst1[0]
                lst1.pop(0)
                autoRemoveSameIdRoom(mini["id"], lst2)
            else:
                mini = lst2[0]
                lst2.pop(0)
            return mini
        def autoRemoveSameIdRoom(id, lst):
            for room in lst.copy():
                if(room["id"] == id):lst.remove(room)
        def getReachableNeighbor(room):
            lst = []
            if(MAZE.UP not in room["wid"]):calcF(room, -1, 0, lst)
            if(MAZE.RIGHT not in room["wid"]):calcF(room, 0, 1, lst)
            if(MAZE.DOWN not in room["wid"]):calcF(room, 1, 0, lst)
            if(MAZE.LEFT not in room["wid"]):calcF(room, 0, -1, lst)
            lst.sort(key=(lambda x:x["f"]))
            return lst
        def calcF(room, offsetRow, offsetColumn, lst):
            row = room["row"] + offsetRow
            column = room["column"] + offsetColumn
            if(row < 0 or column < 0):return
            if(row >=self.row or column >= self.column):return
            nextRoom = self.table[row][column].copy()
            nextRoom["step"] = room["step"] + 1
            nextRoom["f"] = self.column + self.row - column -row - 1 + room["step"]
            nextRoom["parent"] = room["id"]
            lst.append(nextRoom)
        def isReached(closeLst):
            ret = False
            for room in closeLst:
                if(room["id"] == self.row * self.column - 1):
                    ret = True
                    break
            return ret
        def getDirectFlag(lastRoomId, currentRoomId):
            r1 = lastRoomId // self.column
            c1 = lastRoomId % self.column
            r2 = currentRoomId // self.column
            c2 = currentRoomId % self.column
            if(r1 > r2):
                flag = MAZE.DIRECT_EMT[MAZE.UP]
            elif(r1 < r2):
                flag = MAZE.DIRECT_EMT[MAZE.DOWN]
            elif(c1 > c2):
                flag = MAZE.DIRECT_EMT[MAZE.LEFT]
            else:
                flag = MAZE.DIRECT_EMT[MAZE.RIGHT]
            return flag
        def track(table):
            room = table[-1]
            self.table[room["row"]][room["column"]]["flag"] = MAZE.DIRECT_EMT[MAZE.RIGHT]
            parentId = room["parent"]
            while(parentId):
                for parent in table:
                    if(parent["id"] == parentId):
                        self.table[parent["row"]][parent["column"]]["flag"] = getDirectFlag(parentId, room["id"])
                        room = parent
                        parentId = room["parent"]
                        break
            self.table[0][0]["flag"] = getDirectFlag(0, room["id"])
        def removeClosedRoom(lst1, lst2):
            lst = lst1.copy()
            for r in lst1:
                for c in lst2:
                    if(r["id"] == c["id"]):lst.remove(r)
            return lst
        start = time.perf_counter()
        openList = []
        closeList = []
        room = self.table[0][0].copy()
        room["step"] = 0
        room["f"] = self.column + self.row - 1
        closeList.append(room)
        while(not isReached(closeList)):
            lst = getReachableNeighbor(room)
            lst = removeClosedRoom(lst, closeList)
            if(len(lst) == 0 and len(openList) == 0):break
            room = getMiniFValueArray(lst, openList)
            openList.extend(lst)
            openList.sort(key=(lambda x:x["f"]))
            closeList.append(room)
        track(closeList)
        self.times[1] = time.perf_counter() - start
    
    def get(self, *, symbol="#", showFlag=False):
        def fixWalls(room):
            id = room["id"]
            if(id < self.column and MAZE.UP not in room["wid"]):room["wid"].append(MAZE.UP)
            if((id % self.column + 1) == self.column and MAZE.RIGHT not in room["wid"]):room["wid"].append(MAZE.RIGHT)
            if(id + self.column >= self.row * self.column and MAZE.DOWN not in room["wid"]):room["wid"].append(MAZE.DOWN)
            if(id % self.column == 0 and MAZE.LEFT not in room["wid"]):room["wid"].append(MAZE.LEFT)
            if(id == 0 and MAZE.LEFT in room["wid"]):room["wid"].remove(MAZE.LEFT)
            if(id == self.row * self.column - 1 and MAZE.RIGHT in room["wid"]):room["wid"].remove(MAZE.RIGHT)
            room["wid"].sort()
        def onDraw(room, symbol):
            walls = [None, "", ""]
            if(MAZE.UP in room["wid"]):
                walls[0] = symbol * 5
            if(MAZE.DOWN in room["wid"]):
                walls[2] = symbol * 5
            else:
                walls[2] = symbol + " " * 3 + symbol
            if(MAZE.LEFT in room["wid"]):
                walls[1] = symbol + " "
            else:
                walls[1] = " " * 2
            if(showFlag and "flag" in room):
                walls[1] += room["flag"] + " "
            else:
                walls[1] += " " * 2
            if(MAZE.RIGHT in room["wid"]):
                walls[1] += symbol
            else:
                walls[1] += " "
            room["walls"] = walls
            return room
        def combine(index, row):
            desc = ""
            for room in row:
                if(room["walls"][index] is not None):desc += room["walls"][index]
            if(desc):desc += "\n"
            return desc
        def drawRooms(data, symbol):
            canvas = []
            for rowArr in data:
                oneRowRooms = []
                for index in range(len(rowArr)):
                    room = rowArr[index]
                    fixWalls(room)
                    if(oneRowRooms and MAZE.RIGHT in oneRowRooms[-1]["wid"] and MAZE.LEFT in room["wid"]):room["wid"].remove(MAZE.LEFT)
                    if(canvas and MAZE.DOWN in canvas[-1][index]["wid"] and MAZE.UP in room["wid"]):room["wid"].remove(MAZE.UP)
                    oneRowRooms.append(onDraw(room, symbol))
                canvas.append(oneRowRooms)
            return canvas
        canvas = drawRooms(self.table,symbol)
        desc = ""
        for row in canvas:
            for index in range(3):
                desc += combine(index, row)
        time = self.times[0]
        if(showFlag):time = self.times[1]
        desc += "Finished in {:.6f} CPU Times".format(time)
        return desc
    
    def exportPic(self, filename, content, *, fonttype,dir=None, suffix=None):
        def splitDirectFrom(line):
            block = u""
            emt = []
            for chr in line:
                if(chr in MAZE.DIRECT_EMT):
                    if(block):emt.append({"msg":block,"color":MAZE.COLOR_EMT[2]})
                    emt.append({"msg":chr,"color":MAZE.COLOR_EMT[3]})
                    block = u""
                else:
                    block += chr
            if(block):emt.append({"msg":block,"color":MAZE.COLOR_EMT[2]})
            return emt
        saveDir = os.path.join(os.path.dirname(os.path.realpath(__file__)))
        fontDir = os.path.join(saveDir, "fonttype")
        if(dir):saveDir = os.path.join(saveDir, dir)
        if(not os.path.exists(saveDir)):os.makedirs(saveDir)
        font = ImageFont.truetype(fonttype,size=20)
        lines = []
        desc = u"There are {} rooms, {} in each row, {} rows in total".format(self.row * self.column, self.column, self.row)
        img_width = int(font.getlength(desc))
        img_height = font.getbbox(desc)[-1]
        maze_width = 0
        lines.append([{"msg":desc, "color":MAZE.COLOR_EMT[1]}])
        for line in content.split("\n"):
            img_width = max(int(font.getlength(line)), img_width)
            maze_width = max(int(font.getlength(line)), img_width)
            img_height += font.getbbox(line)[-1]
            lines.append(splitDirectFrom(line))
        lines[-1][0]["color"] = MAZE.COLOR_EMT[1]
        img_width += MAZE.PADDING * 2
        img_height += MAZE.PADDING *2
        im = Image.new("RGBA", (img_width, img_height), MAZE.COLOR_EMT[0])
        dr = ImageDraw.Draw(im)
        dr.text((MAZE.PADDING, MAZE.PADDING), lines[0][0]["msg"], font=font, fill=lines[0][0]["color"])
        x = (img_width - maze_width)//2
        y = font.getbbox(lines[0][0]["msg"])[-1] + MAZE.PADDING
        for line in lines[1:-1]:
            for block in line:
                dr.text((x,y), block["msg"], font=font, fill=block["color"])
                x+= int(font.getlength(block["msg"]))
            x = (img_width - maze_width)//2
            y += font.getbbox(line[0]["msg"])[-1]
        dr.text((MAZE.PADDING, y), lines[-1][0]["msg"], font=font, fill=lines[-1][0]["color"])
        filename += "-{}_{}".format(self.row, self.column)
        if(suffix):filename += "-" + suffix
        filename += ".png"
        im.save(os.path.join(saveDir, filename))

if __name__=="__main__":
    opts, arg = getopt.getopt(sys.argv[1:], 'r:c:f:')
    row = 5
    column = 5
    filename = None
    for name, value in opts:
        if(name == "-r"):row = int(value)
        if(name == "-c"):column = int(value)
        if(name == "-f"):filename = value
    screen = SCREEN(column * 10 + MAZE.PADDING * 2)
    maze = MAZE(row, column)
    maze.create()
    if(filename):
        currentDir = os.path.join(os.path.dirname(os.path.realpath(__file__)))
        fontDir = os.path.join(currentDir, "fonttype")
        for root, dirs, files in os.walk(fontDir):
            for fontname in files:
                try:
                    maze.exportPic(filename + "-" + fontname, maze.get(), fonttype=os.path.join(root,fontname), dir="pic")
                    maze.exportPic(filename + "-" + fontname + "-AN", maze.get(showFlag=True), fonttype=os.path.join(root,fontname), dir="pic")
                except Exception as e:
                    print("err:", fontname, e)
    else:
        screen.print(maze.get(), maze.get(showFlag=True))
