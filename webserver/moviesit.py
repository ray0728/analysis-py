from http.server import HTTPServer, BaseHTTPRequestHandler
import os
import subprocess
class MediaFile():
    MEIDA_FILE_MIME = [".MP4",".MKV",".WEBM",".AVI"]
    @staticmethod
    def queryMediaFiles(rootpath, list, hidedir=None):
        if(hidedir is None):
            hidedir = rootpath
        for root, dirs, files in os.walk(rootpath):
            for name in files:
                mime = os.path.splitext(name)[-1]
                if(mime.upper() in MediaFile.MEIDA_FILE_MIME):
                    list.append({'name': name, 'path':os.path.join(root, name).replace(hidedir, "", 1)})
            for name in dirs:
                MediaFile.queryMediaFiles(os.path.join(root, name), list, hidedir)
        return list

class MediaPush():
    @staticmethod
    def push(path):
        subprocess.Popen(["D:\Programs\CNTV\CBox\bin\ffmpeg.exe", "-re", "-i", path, "-c:a", "aac", "-q:a", "0.8", "-c:v", "libx264", "-b:v", "900k", "-maxrate", "1000k", "-vf", "scale=-1:720", "-r", "film", "-g", "3", "-f", "flv", "rtmp://stream/live/movie"], shell=True)


class Resquest(BaseHTTPRequestHandler):
    def createTemplate(self, filelist):
        template = "\
            <html><head><title>Title goes here.</title></head>\
            <body><p>This is a test.</p>";
        for file in filelist:
            template += "<table><tr>"
            template += "<td><h4>%s</h4></td>" % file['name']
            template += "<td><a href=\"%s&play\" target=\"_blank\" rel=\"noopener noreferrer\">Play</a></td></tr><tr>" % file['path']
            template += "<td><img src=\"poster.jpg\" alt=\"Poster\"></td>"
            template += "<td>%s</td></tr></table>" % 'name'
        template += "</body></html>"
        return template.encode("utf-8")

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        if(self.path == "/favicon.ico"):
            pass
        elif(self.path.endswith("&play")):
            MediaPush.push(os.path.join(mediadir, self.path).rstrip("&play"))
        else:
            list = MediaFile.queryMediaFiles(mediadir, [])
            self.wfile.write(self.createTemplate(list))
        self.wfile.flush()
        # self.wfile.close()

if __name__ == '__main__':
    mediadir = 'C:\Program Files\WindowsApps\AppUp.IntelGraphicsExperience_1.100.3282.0_x64__8j3eq9eme6ctt\Assets\en-us'
    host = ('localhost', 8080)
    server = HTTPServer(host, Resquest)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.socket.close
        server.server_close