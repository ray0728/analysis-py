from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains as action
from support.dbms.house.resold.helper import SQLHelper
import time, logging, os, sys 
def initDriver():
    driver = webdriver.Remote(
        command_executor="http://chrome:4444/wd/hub",
        desired_capabilities=DesiredCapabilities.CHROME
    )
    driver.implicitly_wait(10)
    return driver

def close(driver):
    driver.close()
    driver.quit()
    
if __name__ == "__main__":
    driver = initDriver()
    city = sys.argv[1]
    helper = SQLHelper(os.getenv("HOUSE_DB_NAME"), city, os.getenv("HOUSE_DB_HOST"), os.getenv("HOUSE_DB_USERNAME"), os.getenv("HOUSE_DB_PASSWORD"))
    helper.clearData()
    try:
        for page in range(1,101):
            driver.get("https://{}.lianjia.com/ershoufang/pg{}".format(city, page))
            cards = driver.find_elements_by_xpath('//div[@class="info clear"]')
            for card in cards:
                house = card.find_element_by_xpath('.//div[@class="flood"]/div[@class="positionInfo"]/a[1]').get_attribute('textContent')
                region = card.find_element_by_xpath('.//div[@class="flood"]/div[@class="positionInfo"]/a[2]').get_attribute('textContent')
                info = card.find_element_by_xpath('.//div[@class="address"]/div[@class="houseInfo"]').get_attribute('textContent').split("|")
                price = card.find_element_by_xpath('.//div[@class="priceInfo"]/div[@class="totalPrice"]/span').get_attribute('textContent')
                helper.insertNewHouse(region, house, info[1].strip().rstrip("平米"),info[4].strip(), price)
        helper.autoUpdateHouseInfo()
    finally:
        helper.close()
        close(driver)
