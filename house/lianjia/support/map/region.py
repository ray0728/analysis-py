from xml.dom.minidom import parseString
import xml.dom.minidom
import requests
class Region:
    GAODE_WEB_KEY = "605ade0f138e5027e10eea632787dd46"
    CITYNAME_MAP = {'cd':'成都', 'bj':'北京', 'sh':'上海', 'gz':'广州', 'sz':'深圳'}
    @staticmethod
    def translatAddr(cityid, *args):
        location = None
        address = ""
        for index in range(len(args)):
            address = "{} {}".format(address, args[index])
        address = address.strip()
        ret = requests.get("https://restapi.amap.com/v3/geocode/geo?address={} {}&output=XML&key={}".format(Region.CITYNAME_MAP[cityid], address, Region.GAODE_WEB_KEY))
        if(ret.status_code == 200):
            doc = parseString(ret.text)
            collection = doc.documentElement
            location = collection.getElementsByTagName("location")
            if(len(location) > 0):
                location = location[0].childNodes[0].data.split(',')
        return location
