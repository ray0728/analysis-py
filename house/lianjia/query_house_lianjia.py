#coding:utf-8
import sys, os, json, functools 
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from support.dbms.house.resold.helper import SQLHelper

if __name__ == "__main__":
    params = sys.argv[1].strip().split('&')   
    helper = SQLHelper(os.getenv("HOUSE_DB_NAME"), params[0], os.getenv("HOUSE_DB_HOST"), os.getenv("HOUSE_DB_USERNAME"), os.getenv("HOUSE_DB_PASSWORD"))
    heatdata = []
    if(len(params) < 3):
        regionInfo = helper.getAllRegionInfo()
        for info in regionInfo:
            if(params[1].lower() == 'index'):
                heat = int(info['total']/info['count']* 0.5 + info['count']*0.5)
            elif(params[1].lower() == 'total'):
                heat = int(info['total'])
            elif(params[1].lower() == 'max'):
                heat = int(info['max'])
            elif(params[1].lower() == 'aver'):
                heat = int(info['total']/info['count'])
            elif(params[1].lower() == 'count'):
                heat = int(info['count'])
            heatdata.append({"lng":info['lon'], "lat":info['lat'],"count":heat})
    else:
        regionInfo = helper.getAllHouseInfoInRegion(params[2])
        for region in regionInfo:
            averg = region['max']/region['count']
            for info in region['list']:
                if(info['price'] <= averg):
                    styleid = 2
                elif(info['price'] == region['max']):
                    styleid = 0
                else:
                    styleid = 1
                heatdata.append({"lnglat":[info['lon'], info['lat']], "name":"{} {} {}平米 售价{}万元".format(info['house'], info['height'], info['area'], info['price']), 'id':1, 'style':styleid})
    helper.close()
    print(json.dumps(heatdata, separators=(',',':')))
