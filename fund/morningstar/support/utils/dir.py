import os

class Dir:
    def autoCreateDir(*dirs):
        path = ''
        for dir in dirs:
            path = os.path.join(dir)
        if(not os.path.exists(path)):
            os.makedirs(path)
        return path
