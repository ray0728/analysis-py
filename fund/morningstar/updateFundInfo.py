import os,sys
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from support.website.morningstar import MorningStar 

def initDriver():
    driver = webdriver.Remote(
        command_executor="http://chrome:4444/wd/hub",
        desired_capabilities=DesiredCapabilities.CHROME
    )
    driver.implicitly_wait(10)
    return driver

def close(driver):
    driver.close()
    driver.quit()


if __name__ == "__main__":
    driver = initDriver()
    ms = MorningStar(driver)
    try:
        if(ms.login(os.getenv("MS_USERNAME"), os.getenv("MS_PASSWORD"), None)):
            ms.refluseFundInfo(sys.argv[1] == "True", sys.argv[2], sys.argv[3])
    finally:
        ms.close()
        close(driver)
