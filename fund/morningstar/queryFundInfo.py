import os,sys,json
from support.dbms.helper import SQLHelper


if __name__ == "__main__":
    fundlist = []
    params = sys.argv[1].strip().split("&")
    helper = SQLHelper(os.getenv("MS_DB_NAME"), os.getenv("MS_DB_HOST"), os.getenv("MS_DB_USERNAME"), os.getenv("MS_DB_PASSWORD"))
    try:
        funds = helper.getTopFunds(params[0], params[1], params[2], int(params[3]))
        for fund in funds:
            fundlist.append({'code':fund[0], 'mscode':fund[1], 'name':fund[2], 'type':fund[3], 'front':fund[4], 'redeem':fund[5], 'subscribe':fund[6], 'rr':fund[7], 'tr':fund[8], 'fr':fund[9], 'min':fund[10], 'beta':fund[11]})
    finally:
        helper.close()
    print(json.dumps({'fund':fundlist}, separators=(',',':')))
